import React from 'react';
import './CountriesName.css';

const CountriesName = (props) => {
    return (
        <div className="block clearfix">
            <div className="country-block">
                {props.countriesName.map((country, id) => {
                    return(
                        <div key={id} className="country-name"  onClick={() => props.clicked(country.name)}><p>{country.name}</p></div>
                    )
                })}
            </div>
        </div>
    );
};

export default CountriesName;