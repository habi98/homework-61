import React from 'react';
import './InfoCountry.css';

const InfoCountry  = (props) => {
    if(!props.infoCountry) return null;
    return (
        <div className="Info-block clearfix">
            <div className="Info">
                <h3 className="title-name">{props.infoCountry.name} </h3>
                <p className="capital">Capital: {props.infoCountry.capital}</p>
                <p>Population: {props.infoCountry.population} M</p>
                <div className="border">
                    <p>Border with: </p>
                    <ul>
                       {props.infoCountry.borders.map((border, id) =>  <li key={id}>{border}</li>)}
                    </ul>
                </div>
            </div>
            <div className="block-img">
                <img className="img-wrap" src={props.infoCountry.flag} alt=""/>
            </div>
        </div>
    );
};




export default InfoCountry;