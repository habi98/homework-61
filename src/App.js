import React, { Component, Fragment } from 'react';

import axios from 'axios'
import CountriesName from "./components/CountriesName/CountriesName";
import InfoCountry from "./components/InfoCountry/InfoCountry";

class App extends Component {
  state = {
      countriesName: [],
      loadedCountry: null
  };

    componentDidMount() {
      const Name_URL ='all?fields=name;alpha3Code';

      axios.get(Name_URL).then(response => {this.setState({countriesName: response.data})
      }).catch(error => {
          console.log(error);
      })
  }

  getCountryInfo = (name) => {
      axios.get(`/name/${name}`).then(response => {
          const country = response.data[0];
          console.log(response.data[0]);
          Promise.all(country.borders.map(border => axios.get(`/alpha/${border}`)))
              .then(borderResponse => {
                  country.borders = borderResponse.map(border => border.data.name);
                  this.setState({loadedCountry: country})
              })
       })
  };
    render() {
        return (
            <Fragment>
                <CountriesName
                    countriesName={this.state.countriesName}
                    clicked={this.getCountryInfo}
                />
                <InfoCountry infoCountry={this.state.loadedCountry}/>
            </Fragment>

    );
  }
}

export default App;
